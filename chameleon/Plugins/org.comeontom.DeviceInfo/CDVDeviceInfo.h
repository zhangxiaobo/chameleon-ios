//
//  CDVDeviceInfo.h
//  chameleon
//
//  Created by comeontom on 14/9/12.
//
//

#import "CDVPlugin.h"

@interface CDVDeviceInfo : CDVPlugin

- (void)getDeviceId:(CDVInvokedUrlCommand*)command;
- (void)getDeviceDetail:(CDVInvokedUrlCommand*)command;
- (void)getIp:(CDVInvokedUrlCommand*)command;

- (void)setIp:(CDVInvokedUrlCommand*)command;


- (void)getDeviceInfo:(CDVInvokedUrlCommand*)command;

@end
