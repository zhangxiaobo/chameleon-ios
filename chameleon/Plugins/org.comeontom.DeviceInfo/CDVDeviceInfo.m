//
//  CDVDeviceInfo.m
//  chameleon
//
//  Created by comeontom on 14/9/12.
//
//

#import "CDVDeviceInfo.h"
#import "OpenUDID.h"


@interface CDVDeviceInfo ()
@property(nonatomic,strong)NSString *token;
@property(nonatomic,strong)NSString *udid;
@end

@implementation CDVDeviceInfo
static NSString* URL_MDM = @"http://115.28.0.60";

- (void)getDeviceId:(CDVInvokedUrlCommand*)command{
//    NSLog(@"comeontom");
    NSLog(@"%@",command);
//    NSString* deviceId = [OpenUDID value];
    NSDictionary *configurationDic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"com.apple.configuration.managed"];
    if (configurationDic) {
        NSArray *settingsArr = [configurationDic valueForKey:@"Settings"];
        NSDictionary *settingsDic = settingsArr[0];
        NSDictionary *settingsConfigurationDic = [settingsDic objectForKey:@"Configuration"];
        
        self.token = [settingsConfigurationDic objectForKey:@"token"];
        self.udid = [settingsConfigurationDic objectForKey:@"udid"];
    }else{
        self.udid = [OpenUDID value];
    }
    

    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:self.udid];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
- (void)getDeviceDetail:(CDVInvokedUrlCommand*)command{
    NSString *appId = @"43ea5ef81c2e036a93de068f7dd739be";
    NSString *channelId = @"apns";
    NSString *deviceName = [[UIDevice currentDevice] name];//设备名称
//    NSString *model = [[UIDevice currentDevice]localizedModel];
    
//    NSString *deviceModelName = [NSString stringWithFormat:@"%@_%@",deviceName,model];
    
    NSString *osName = @"ios";
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];//手机系统版本
    
    NSString *pushuToken;
    if (self.token) {
        pushuToken = self.token;
    }else{
        pushuToken = @"";
    }
    
    
    NSDictionary *deviceDetailDic = @{@"appId": appId,
                                      @"channelId":channelId,
                                      @"deviceName":deviceName,
                                      @"osName":osName,
                                      @"osVersion":osVersion,
                                      @"pushToken":pushuToken};
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:deviceDetailDic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSLog(@"JSON String = %@", jsonString);
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getIp:(CDVInvokedUrlCommand*)command{
    NSString *ip = URL_MDM;
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:ip];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)setIp:(CDVInvokedUrlCommand*)command{
    URL_MDM = command.arguments[0];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)getDeviceInfo:(CDVInvokedUrlCommand*)command{
    NSDictionary *deviceDetailDic = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    
//    NSDictionary *configurationDic = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"com.apple.configuration.managed"];
//    NSArray *settingsArr = [configurationDic valueForKey:@"Settings"];
//    NSDictionary *settingsDic = settingsArr[0];
//    NSDictionary *settingsConfigurationDic = [settingsDic objectForKey:@"Configuration"];
//    NSString *token = [settingsConfigurationDic objectForKey:@"token"];
//    NSString *udid = [settingsConfigurationDic objectForKey:@"udid"];
    
    NSData *jsonData = [NSJSONSerialization
                        dataWithJSONObject:deviceDetailDic options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
    [pluginResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}
@end
