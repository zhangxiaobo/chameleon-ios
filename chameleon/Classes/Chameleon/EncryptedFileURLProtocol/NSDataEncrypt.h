//
//  NSDataEncrypt.h
//  aestest
//
//  Created by yaoziyang on 14-7-15.
//  Copyright (c) 2014年 yaoziyang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (md5)

// return 16 bytes md5 stream for this string
-(NSData *) md5Data;

@end

@interface NSData(AESUtilitiy)

/*
 * @Desc: aes-128-ecb-pkcs5padding encrypt, may throw NSException
 * @Note: key lenght must be 16, 24, or 32.
 * @Usage:for example, our key is 'abcdefg', data is a unencrypt NSData Object,
 *        to encrypt data, we can use [data aesEncrypt:[key md5Data]]
 */
- (NSData*)aesEncrypt:(NSData*)key;//加密
- (NSData*)aesDecrypt:(NSData*)key;//解密

/*
 * see aesEncrypt
 */
+ (NSData*)aesEncryptFile:(NSURL*)url key:(NSData*)key;
+ (NSData*)aesDecryptFile:(NSURL*)url key:(NSData*)key;

// convert hex string like 'e37eb8ba26bd05' to byte stream
+ (NSData*)parseHexString:(NSString*)str;

@end
