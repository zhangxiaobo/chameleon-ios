//
//  NSOutputStream+Swizzle.m
//  chameleon
//
//  Created by comeontom on 14/8/22.
//
//

#import "NSOutputStream+Swizzle.h"
#import "JRSwizzle.h"
#import "AESOutputStream.h"


@implementation NSOutputStream (Swizzle)

+ (void)load{
    [self jr_swizzleClassMethod:@selector(outputStreamToFileAtPath:append:) withClassMethod:@selector(SWOutputStreamToFileAtPath:append:) error:nil];
}

+ (id)SWOutputStreamToFileAtPath:(NSString *)path append:(BOOL)shouldAppend{
    NSOutputStream* outputStream = [self SWOutputStreamToFileAtPath:path append:shouldAppend];
    
    /**
     *  在这里判断是否需要进行AES加密保存文件
     */
    NSString *app = @"/app/";
    NSRange foundObj=[path rangeOfString:app options:NSCaseInsensitiveSearch];
    if(foundObj.length>0) {
        AESOutputStream* aesoutputStream = [[AESOutputStream alloc]initWithOutputStream:outputStream];
        return aesoutputStream;
    }
    return outputStream;
}
@end
