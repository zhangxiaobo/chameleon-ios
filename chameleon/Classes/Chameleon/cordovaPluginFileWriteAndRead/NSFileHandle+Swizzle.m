//
//  NSFileHandle+Swizzle.m
//  NSStreamTest2
//
//  Created by comeontom on 14/8/25.
//  Copyright (c) 2014年 comeontom. All rights reserved.
//

#import "NSFileHandle+Swizzle.h"
#import "JRSwizzle.h"
#import "AESFileHandle.h"

@implementation NSFileHandle (Swizzle)

+ (void)load{
    [NSFileHandle jr_swizzleClassMethod:@selector(fileHandleForReadingAtPath:) withClassMethod:@selector(SWFileHandleForReadingAtPath:) error:nil];
}

+ (id)SWFileHandleForReadingAtPath:(NSString *)path{
    NSLog(@"SWFileHandleForReadingAtPath");
    id fileHandle = [self SWFileHandleForReadingAtPath:path];
    /**
     *  在这里判断是否需要进行AES加密保存文件
     */
    NSString *app = @"/app/";
    NSRange foundObj=[path rangeOfString:app options:NSCaseInsensitiveSearch];
    if(foundObj.length>0) {
        AESFileHandle* aesFileHandle =[[AESFileHandle alloc]initWithFileHandle:fileHandle];
        return aesFileHandle;
    }
    
    return fileHandle;
}

@end
