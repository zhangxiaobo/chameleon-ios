//
//  AESFileHandle.m
//  NSStreamTest2
//
//  Created by comeontom on 14/8/25.
//  Copyright (c) 2014年 comeontom. All rights reserved.
//

#import "AESFileHandle.h"

#import "EncryptedFileURLProtocol.h"
#import "NSDataEncrypt.h"


@interface AESFileHandle()
@property(nonatomic,strong)NSFileHandle *fileHandle;
@end

@implementation AESFileHandle

-(instancetype)initWithFileHandle:(NSFileHandle *)delegate{
    if (self = [super init]) {
        self.fileHandle = delegate;
    }
    return self;
}
//+ (instancetype)sharedInstance:(NSFileHandle *)delegate
//{
//    static AESFileHandle *sharedFileHandle = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedFileHandle = [[AESFileHandle alloc] init];
//    });
//    sharedFileHandle->_fileHandle = delegate;
//    return sharedFileHandle;
//}

- (void)seekToFileOffset:(unsigned long long)offset{
    [self.fileHandle seekToFileOffset:offset];
}

- (NSData *)readDataToEndOfFile{
    NSData *data = [self.fileHandle readDataToEndOfFile];
    
    NSData *decryptData = [data aesDecrypt:[[EncryptedFileURLProtocol key]md5Data]];
    return decryptData;
}

- (NSData *)readDataOfLength:(NSUInteger)length{
    NSData *data = [self.fileHandle readDataOfLength:length];
    NSData *decryptData = [data aesDecrypt:[[EncryptedFileURLProtocol key]md5Data]];
    return decryptData;
}

- (void)closeFile{
    [self.fileHandle closeFile];
}


@end
