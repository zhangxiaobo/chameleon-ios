//
//  AESOutputStream.h
//  NSStreamTest
//
//  Created by comeontom on 14/8/22.
//  Copyright (c) 2014年 comeontom. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AESOutputStream : NSOutputStream
-(instancetype)initWithOutputStream:(NSOutputStream *)delegate;
@end
