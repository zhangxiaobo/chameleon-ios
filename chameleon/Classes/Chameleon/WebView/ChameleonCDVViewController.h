//
//  ChameleonCDVViewController.h
//  chameleon
//
//  Created by Justin Yip on 6/18/14.
//
//

#import <Cordova/CDVViewController.h>
#import <Cordova/CDVCommandQueue.h>

@interface ChameleonCDVViewController : CDVViewController

@end

@interface ChameleonCommandQueue : CDVCommandQueue
@end