//
//  ChameleonCommandDelegate.h
//  chameleon
//
//  Created by Justin Yip on 7/2/14.
//
//

#import <Cordova/CDVCommandDelegateImpl.h>

@interface ChameleonCommandDelegate : CDVCommandDelegateImpl

@end
