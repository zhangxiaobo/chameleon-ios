//
//  ChameleonCommandDelegate.m
//  chameleon
//
//  Created by Justin Yip on 7/2/14.
//
//

#import "ChameleonCommandDelegate.h"
#import "NSFileManager+Chameleon.h"
#import "CApplication.h"

@implementation ChameleonCommandDelegate

/* To override the methods, uncomment the line in the init function(s)
 in MainViewController.m
 */

#pragma mark CDVCommandDelegate implementation

- (id)getCommandInstance:(NSString*)className
{
    return [super getCommandInstance:className];
}

/*
 NOTE: this will only inspect execute calls coming explicitly from native plugins,
 not the commandQueue (from JavaScript). To see execute calls from JavaScript, see
 MainCommandQueue below
 */
//- (BOOL)execute:(CDVInvokedUrlCommand*)command
//{
//    return [super execute:command];
//}

- (NSString*)pathForResource:(NSString*)resourcepath;
{
    //Sandbox模式下，以Documents/app文件夹为基础
    return [[[[NSFileManager documentsDirectory]
              URLByAppendingPathComponent:kWWWFolderName isDirectory:YES]
             URLByAppendingPathComponent:resourcepath] relativePath];
}

@end
