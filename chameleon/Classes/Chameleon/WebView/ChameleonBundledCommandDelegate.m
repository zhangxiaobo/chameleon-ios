//
//  ChameleonCommandDelegate.m
//  chameleon
//
//  Created by Justin Yip on 7/2/14.
//
//

#import "ChameleonCommandDelegate.h"

@implementation ChameleonCommandDelegate

/* To override the methods, uncomment the line in the init function(s)
 in MainViewController.m
 */

#pragma mark CDVCommandDelegate implementation

- (id)getCommandInstance:(NSString*)className
{
    return [super getCommandInstance:className];
}

/*
 NOTE: this will only inspect execute calls coming explicitly from native plugins,
 not the commandQueue (from JavaScript). To see execute calls from JavaScript, see
 MainCommandQueue below
 */
//- (BOOL)execute:(CDVInvokedUrlCommand*)command
//{
//    return [super execute:command];
//}

- (NSString*)pathForResource:(NSString*)resourcepath;
{
    //读取安装包www内容
    return [super pathForResource:resourcepath];
}

@end
