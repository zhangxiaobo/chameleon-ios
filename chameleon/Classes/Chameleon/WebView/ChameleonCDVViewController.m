//
//  ChameleonCDVViewController.m
//  chameleon
//
//  Created by Justin Yip on 6/18/14.
//
//

#import "ChameleonCDVViewController.h"
#import "NSFileManager+Chameleon.h"
#import <ReactiveCocoa.h>
#import "CApplication.h"
#import "ChameleonCommandDelegate.h"

@interface ChameleonCDVViewController ()

@end

@implementation ChameleonCDVViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        //ChameleonCommandDelegate，不同的target使用不同的实现
        _commandDelegate = [[ChameleonCommandDelegate alloc] initWithViewController:self];
        self.wwwFolderName = kWWWFolderName;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.webView.keyboardDisplayRequiresUserAction = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIWebDelegate implementation

- (void)webViewDidFinishLoad:(UIWebView*)theWebView
{
    // Black base color for background matches the native apps
    theWebView.backgroundColor = [UIColor blackColor];
    
    return [super webViewDidFinishLoad:theWebView];
}

/* Comment out the block below to over-ride */
 
- (void) webViewDidStartLoad:(UIWebView*)theWebView
{
    return [super webViewDidStartLoad:theWebView];
}

- (void) webView:(UIWebView*)theWebView didFailLoadWithError:(NSError*)error
{
    [super webView:theWebView didFailLoadWithError:error];
    
//    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"错误" message:@"无法启动，是否进行初始化修复？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
//    [av show];
//    [av.rac_buttonClickedSignal subscribeNext:^(id x) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:kAppResetNotification object:nil];
//    }];
}

- (BOOL) webView:(UIWebView*)theWebView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
    return [super webView:theWebView shouldStartLoadWithRequest:request navigationType:navigationType];
}

@end

@implementation ChameleonCommandQueue

/* To override, uncomment the line in the init function(s)
 in MainViewController.m
 */
- (BOOL)execute:(CDVInvokedUrlCommand*)command
{
    return [super execute:command];
}

@end
