//
//  NSFileManager+Chameleon.h
//  chameleon
//
//  Created by Justin Yip on 6/16/14.
//
//

#import <Foundation/Foundation.h>

@interface NSFileManager (Chameleon)

// app/ in app bundle
+ (NSURL *)bundledAppDirectory;

// Documents directory
+ (NSURL *)documentsDirectory;

// Documents里的web运行时目录
+ (NSURL *)webRuntimeDirectory;

@end
