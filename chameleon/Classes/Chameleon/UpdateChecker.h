//
//  UpdateChecker.h
//  chameleon
//
//  Created by Justin Yip on 6/17/14.
//
//

#import <Foundation/Foundation.h>

@interface UpdateChecker : NSObject<UIAlertViewDelegate>

+ (void)check;

@end
