//
//  ZillaClient.h
//  chameleon
//
//  Created by Justin Yip on 6/19/14.
//
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>
#import <AdSupport/ASIdentifierManager.h>

extern NSString* const kAppKey;
extern NSString* const kServerUrl;
extern NSString* const kBslDownloadUrl;

@interface ZillaClient : NSObject

//文件下载路径
+(NSString*)URLStringForFile:(NSString*)identifier;

//获取本应用最新版本
+(RACSignal*)getLastestAppVersion;

//应用更新地址，参数为plist文件的唯一ID
+(NSString*) itmsURLStringForAppPlistFileID:(NSString *)package;

//应用验证,获取token
+(NSString*) validateApplication;

//应用签到
+ (void) checkInWithToken: (NSString *)token done: (void(^)(id response)) done;

@end
