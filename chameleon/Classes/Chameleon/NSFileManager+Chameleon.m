//
//  NSFileManager+Chameleon.m
//  chameleon
//
//  Created by Justin Yip on 6/16/14.
//
//

#import "NSFileManager+Chameleon.h"
#import "CApplication.h"

@implementation NSFileManager (Chameleon)

//应用文档根目录
+ (NSURL *)documentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

+ (NSURL *)bundledAppDirectory
{
    return [[NSBundle mainBundle] URLForResource:@"app" withExtension:nil];
}

+ (NSURL *)webRuntimeDirectory
{
    return [[self documentsDirectory] URLByAppendingPathComponent:kWWWFolderName isDirectory:YES];
}

@end
