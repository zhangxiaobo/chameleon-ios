# Chameleon-iOS
欢迎使用变色龙iOS！

忠告，业务功能请尽量使用H5实现，以便保持应用轻量，有利于日后升级迁移。

## 运行
首先使用辅助脚本拉取html5代码，然后使用xcode运行，或使用./cordova/run在命令行运行。

注：每次html5代码有更新，请务必执行clean操作，以防代码不生效的情况方式。

## 辅助脚本
Chameleon-iOS提供prepare-app辅助脚本，方便进行html5代码自动化更新。

### 运行模式
* ./prepare-app -b 使用bundled模式
* ./prepare-app -s 使用sandbox模式

默认使用bundled模式

### 配置(profile)
使用 -p 参数切换到不同的配置，默认为production。

例子：
* ./prepare-app 使用bundled模式，production profile
* ./prepare-app -s 使用sandbox模式，production profile
* ./prepare-app -b -p 使用bundled模式，development profile

注：
* prepare-app为node.js脚本，请确保已安装node.js运行时。
* 代码路径请直接修改prepare-app脚本
* 运行./prepare-app --help查看帮助

## html5相关目录
有两个特殊目录：
* www

Chameleon-iOS基于Cordova框架，为便于日后对插件进行添加，升级等维护操作。

请保持此文件夹干净，只存放cordova.js, cordova_plugins 以及 plugins /目录
* app

存放你的html5代码

## 运行模式
变色龙可以以两种模式运行，Chameleon-iOS以两个target的方式，对这两种模式提供支持：

### bundled模式

使用“chameleon-bundled” target

html5应用捆绑在应用中运行，该模式即为传统混合式App，具有开发快速开发，方便调试等特点。

运行行为：启动时会将www目录下的所有文件copy到app目录下。


### sandbox模式

使用“chameleon-sandbox” target

html5应用使用沙盒模式运行，同样为混合式App，但使用独立的运行时，能够为html5提供独立模块更新，安装等特性。

首次启动时，会将www以及app目录，合并复制到Documents/{kWWWFolderName}目录下作为运行时目录。（kWWWFolderName常量在CApplication.h头文件定义）

应用日后运行均以*运行时目录*为基础。模块更新，安装，删除等行为，也在运行时目录发送。

应用整体升级时，可能包含比运行时目录更新的html5模块，应用会对应用捆绑目录下的模块与运行时目录的模块进行比对，在应用捆绑目录下筛选出新的和版本较新的模块，复制/覆盖到运行时目录下。

# 启动页面路径

基准目录：启动页面首先由运行模式决定，bundled模式基于应用包的app目录，sandbox模式基于Documents/{kWWWFolderName}目录

然后是config.xml下的content属性，默认是<content src="index.html" />，修改为你的启动页路径即可。

# 添加cordova插件

plugman install --platform ios --project . --plugin org.apache.cordova.file